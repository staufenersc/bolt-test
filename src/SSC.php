<?php

namespace App;

use Bolt\Extension\BaseExtension;
use Symfony\Component\Routing\Route;

class SSC extends BaseExtension
{
    /**
     * Return the full name of the extension
     */
    public function getName(): string
    {
        return 'Bolt Erweiterung.';
    }

    public function initialize(): void
    {
        $data = $this->getEvents();

        dump($data);
    }

    public function getEvents(): array
    {
        $fetcher = new SscDataFetcher($this->getQuery());
        return $fetcher->fetch();
    }

    private function getConfigValue($config): string
    {
        if (! $this->extension) {
            return '';
        }

        return (string) $this->extension->getConfig()->get($config);
    }
}

use Bolt\Enum\Statuses;
use Bolt\Storage\Query;
use \DateTime;
use \DateInterval;

class SscDataFetcher
{
    private $query;

    public function __construct(Query $query)
    {
        $this->query = $query;
    }

    public function fetch(): array
    {
        // Using the generic Query::getContent()
        $rawteams = $this->query->getContent('teams', [
            'status' => Statuses::PUBLISHED,
        ]);
        $teams = iterator_to_array($rawteams->getCurrentPageResults());

        // collector
        $entries = [];

        // get games
        $entries = array_merge($entries,$this->getGames());

        // get events
        $entries = array_merge($entries,$this->getEvents());

        // Using Query::getContentForTwig()
        // which sets default parameters, like status 'published'
        // and orderBy
        // $entries = $this->query->getContentForTwig('games', [
        //     'headline' => '%entry%', // search LIKE
        // ]);

        // Get entries and pages, without any filtering parameters
        // $gamesAndEvents = $this->query->getContentForTwig('games,events');

        /*
        // Per default PagerFanta only loads 20 items per page.
        // We do not want paging so we set value to '250'
        $entries->setMaxPerPage(250);
        // Finally, since all results are paginated using PagerFanta
        // here is a few operations we can run on them.
        $numberOfPages = $entries->getNbResults();
        $numberOfEntries = $entries->getNbResults();
        $currentPage = $entries->getCurrentPage();
        $currentPageEntries = iterator_to_array($entries->getCurrentPageResults());

        // And finally, let's just return entries that we found
        // return $currentPageEntries;
        return $currentPageEntries;
        */
        return $entries;
        // return [];
    }

    public function getGames(): array
    {
        $sscgames = [];

        $entries = $this->query->getContent('games', [
            'status' => Statuses::PUBLISHED,
            'datefrom' => '>='.$this->helper()->today,
        ]);
        $entries->setMaxPerPage(200);

        foreach($entries as $entry)
        {
            $e_datefrom = new DateTime($entry->getFieldValue('datefrom'));
            $e_headline = $entry->getFieldValue('headline') ?? '';

            if ($e_datefrom->format('Y-m-d') >= $this->helper()->today)
            {
                $item = (object) [
                    'id' => $entry->getId(),
                    'group' =>'games',
                    'date' => $e_datefrom,
                    'headline' => $e_headline,
                ];

                $sscgames[] = $item;
            }
        }

        return $sscgames;
    }

    public function getEvents(): array
    {
        $sscevents = [];

        $entries = $this->query->getContent('events', [
            'status' => Statuses::PUBLISHED,
            'datefrom' => '>='.$this->helper()->today,
        ]);
        $entries->setMaxPerPage(200);

        foreach($entries as $entry)
        {
            $e_datefrom = new DateTime($entry->getFieldValue('datefrom'));
            $e_headline = $entry->getFieldValue('headline') ?? '';

            if ($e_datefrom->format('Y-m-d') >= $this->helper()->today)
            {
                $item = (object) [
                    'id' => $entry->getId(),
                    'group' =>'events',
                    'date' => $e_datefrom,
                    'headline' => $e_headline,
                ];

                $sscevents[] = $item;
            }
        }

        return $sscevents;
    }


    public function helper(): object
    {
        $datetime = new DateTime('today');
        $today = $datetime->format('Y-m-d');
        $datetime = new DateTime('tomorrow');
        $tomorrow = $datetime->format('Y-m-d');
        $weekdays = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'];
        $weekdays_en = [ 'Montag' => 'monday', 'Dienstag' => 'tuesday', 'Mittwoch' => 'wednesday', 'Donnerstag' => 'thursday', 'Freitag' => 'friday', 'Samstag' => 'saturday', 'Sonntag' => 'sunday' ];

        return (object) [
            'today' => $today,
            'tomorrow' => $tomorrow,
            'weekdays' => $weekdays,
            'weekdays_en' => $weekdays_en,
            'today_css' => 'today',
            'today_label' => 'Heute',
            'tomorrow_label' => 'Morgen',
            'timeline_length_default' => '780',
            'timeline_start_default' => '09:00',
        ];
    }
}

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SscTwig extends AbstractExtension
{
    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        dump('SSC /src');
        return [
            new TwigFunction(
                'testMethod',
                [$this, 'testMethodAction']
            ),
        ];
    }

    /**
     * @return string
     */
    public function testMethodAction(): string
    {
        // --> return the fetched data here??
        return 'Hello World!';
    }
}
