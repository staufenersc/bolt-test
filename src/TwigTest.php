<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigTest extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('testFunction', [$this, 'testFunction']),
        ];
    }

    public function testFunction()
    {
        return 'some-test-string';
    }
}
